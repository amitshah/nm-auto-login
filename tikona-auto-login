#!/usr/bin/python

# Script to log into the login.tikona.in page
#
# Needs a config file with username and password in
# /etc/network-auto-logins or in a user's home dir:
# $HOME/etc/network-auto-logins
#
# The config file format is:
#   [Tikona]
#   username: xyz
#   password: 123
#
# Ensure to adjust the permissions of the config file so that only
# root or the user has read/write access.
#
# Author: Amit Shah <amitshah@gmx.net>
# Copyright (C) 2010, Amit Shah <amitshah@gmx.net>
#
# Distributed under the GPL v2, see the file COPYING for details.

import urllib, urllib2, cookielib, ConfigParser, os
from re import search

debug = 0

config = ConfigParser.RawConfigParser()
cfg_file = config.read(['/etc/network-auto-logins',
                        os.path.expanduser('~/etc/network-auto-logins')])
if not cfg_file:
    print "error: no accessible config file found"
    exit(1)

try:
    username = config.get('Tikona', 'username')
    password = config.get('Tikona', 'password')
except:
    print "error: no username or password specified in the 'Tikona' section"
    print "error: using config file(s) ",
    print cfg_file
    exit(2)

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

try:
    f = opener.open("http://google.com")
    s = f.read()
except:
    # network is up, but can't resolve hostnames?
    print "error: cannot reach google.com"
    exit(3)

refresh_url_pattern = "HTTP-EQUIV=\"Refresh\" CONTENT=\"2;URL=(.*)\">"
refresh_url = search(refresh_url_pattern, s)

if not refresh_url:
    print "error: no refresh url obtained"
    if debug:
        print s
    exit(0)

if debug:
    print refresh_url.group(1)

if not search(".*tikona.*", refresh_url.group(0)):
    print "not on tikona network"
    exit(0)

base_url_pattern = "(http.*/)(\?.*)$"
base_url = search(base_url_pattern, refresh_url.group(0))

if not base_url:
    print "error: couldn't get baseurl"
    if debug:
        print refresh_url.group(1)
    exit(4)

if debug:
    print "baseurl: " + base_url.group(1)

f = opener.open(refresh_url.group(1))
s = f.read()

load_form_pattern = ".*action=\"(.*)\";"
load_form_id = search(load_form_pattern, s)
load_form_url = base_url.group(1) + load_form_id.group(1)

if debug:
    print s
    print "load form id: " + load_form_id.group(1)
    print "load_form_url: " + load_form_url

f = opener.open(load_form_url)
if debug:
    s = f.read()
    print s

# This comes from cookie.js, hardcoding here
#
# TODO: Read all the js files and find out what the action is for
# the savesetting() function.
#
# TODO: Also find out which function is invoked on submit:
# currently it's the savesetting() function.
#
login_form_id = "newlogin.do?phone=0"
type = "2"

login_form_url = base_url.group(1) + login_form_id
if debug:
    print "Using login form url: " + login_form_url

login_data = urllib.urlencode({'username': username, 'password': password,
                               'type': type})

f = opener.open(login_form_url, login_data)
if debug:
    s = f.read()
    print s

exit(0)
